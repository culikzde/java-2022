package example2;

import java.awt.*;
import java.awt.event.*;
import java.lang.reflect.*;
import java.beans.*;
import java.util.logging.*;
import javax.swing.*;

public class Window extends javax.swing.JFrame {

    public Window() {
        initComponents();
        display (jButton1);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        propField = new javax.swing.JTextField();
        valueField = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        setButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jButton1.setText("jButton1");
        jButton1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jButton1MousePressed(evt);
            }
        });
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jTextArea1.setColumns(20);
        jTextArea1.setFont(new java.awt.Font("Monospaced", 0, 18)); // NOI18N
        jTextArea1.setRows(5);
        jScrollPane1.setViewportView(jTextArea1);

        propField.setText("text");

        valueField.setText("Abc");
        valueField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                valueFieldActionPerformed(evt);
            }
        });

        jLabel1.setText(" property name");

        jLabel2.setText("propery value");

        setButton.setText("set");
        setButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                setButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(80, 80, 80)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2))
                        .addGap(37, 37, 37)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(propField, javax.swing.GroupLayout.DEFAULT_SIZE, 183, Short.MAX_VALUE)
                            .addComponent(valueField))
                        .addGap(18, 18, 18)
                        .addComponent(setButton)
                        .addGap(0, 132, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(23, 23, 23)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(propField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1)
                            .addComponent(setButton))
                        .addGap(8, 8, 8)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(valueField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2))))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 454, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(21, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        jButton1.setText ("Click");

    }//GEN-LAST:event_jButton1ActionPerformed

    private void put (String s)
    {
        jTextArea1.append (s + "\n");
    }
    
    Object display_obj = null;

    private void display (Object obj)
    {
        display_obj = obj;
        Class cls = obj.getClass();
        // import java.beans.*;
        try 
        {
            BeanInfo info = Introspector.getBeanInfo (cls);
            PropertyDescriptor [] list = info.getPropertyDescriptors();
            for (PropertyDescriptor prop : list)
            {
                String t = "";
                Class c = prop.getPropertyType();
                if (c != null)
                    t = c.getSimpleName();
                
                Object v = "";
                Method m = prop.getReadMethod();
                if (m != null)
                    try 
                    {
                        Object [] params = new Object [] {};
                        v = m.invoke (obj, params);
                    } 
                    catch (IllegalAccessException ex) {  } 
                    catch (IllegalArgumentException ex) { } 
                    catch (InvocationTargetException ex) { }
                
                put (prop.getName()  + " : " + t + " = " + v);
            }
            
        } 
        catch (IntrospectionException ex) 
        {
            Logger.getLogger(Window.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
       
    private void jButton1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton1MousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton1MousePressed

    private void valueFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_valueFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_valueFieldActionPerformed

    private void setButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_setButtonActionPerformed
        Object obj = display_obj;
        if (obj != null)
        {
            Class cls = obj.getClass ();
            String set_name = propField.getText ();
            String set_value = valueField.getText ();
            try 
            {
                BeanInfo info = Introspector.getBeanInfo (cls);
                PropertyDescriptor [] list = info.getPropertyDescriptors();
                for (PropertyDescriptor prop : list)
                {
                    if (prop.getName ().equals (set_name))
                    {
                        Method m = prop.getWriteMethod();
                        if (m != null)
                        {
                            String err = "";
                            Object param = set_value;
                            if (prop.getPropertyType() == int.class)
                                param = Integer.parseInt(set_value);
                            Object [] params = new Object [] { param };
                            try 
                            {
                                m.invoke (obj, params);
                            } 
                            catch (Exception ex) { err = ex.toString(); } 
                            if (err.length() != 0)
                                put ("EXCEPTION " + err);
                        }
                    }
                }

            } 
            catch (IntrospectionException ex)  {   }
        }
    }//GEN-LAST:event_setButtonActionPerformed

    private void nic ()
    {
        jButton1.addActionListener
        (
            new java.awt.event.ActionListener() 
            {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    jButton1ActionPerformed(evt);
                }
            }
        );
        
        jButton1.addMouseListener
        (
            new java.awt.event.MouseAdapter() {
                @Override
                public void mousePressed(java.awt.event.MouseEvent evt) {
                    
                   java.awt.event.MouseAdapter a = this;
                   Window b = Window.this;
                   Window.this.jButton1MousePressed(evt);
                }
            }
        );
       
       
        T t = new T ();
    }
    
    public class T
    {
        public void f ()
        {
           T a = this;
           Window w = Window.this;
        }
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        
        Window w = new Window();
        w.setVisible(true);
        T t = w.new T ();

    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTextField propField;
    private javax.swing.JButton setButton;
    private javax.swing.JTextField valueField;
    // End of variables declaration//GEN-END:variables
}
