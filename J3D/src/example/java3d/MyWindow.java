
package example.java3d;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.media.j3d.*;
import javax.vecmath.*;
import com.sun.j3d.utils.geometry.*;
import com.sun.j3d.utils.universe.*;


import com.sun.j3d.utils.image.*;
import com.sun.j3d.utils.behaviors.mouse.*;
import com.sun.j3d.utils.geometry.*;
import com.sun.j3d.utils.universe.*;

public class MyWindow extends JFrame
{
  BorderLayout borderLayout1 = new BorderLayout();
  MyPanel3D myPanel = new MyPanel3D();

  public MyWindow()
  {
    enableEvents(AWTEvent.WINDOW_EVENT_MASK);
    try
    {
      jbInit();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }

    // myPanel.addBranchGroup (createDodecahedron ());

    // myPanel.addBranchGroup (createScene1 ());
    myPanel.addBranchGroup (createScene2 ());
    // myPanel.addBranchGroup (createSphere ());
  }

  private void jbInit() throws Exception
  {
    this.getContentPane().setLayout(borderLayout1);
    this.setSize(new Dimension(400, 300));
    this.setTitle("Java 3D Test");
    this.getContentPane().add(myPanel, BorderLayout.CENTER);
   }


  protected void processWindowEvent(WindowEvent e)
  {
    super.processWindowEvent(e);
    if(e.getID() == WindowEvent.WINDOW_CLOSING)
    {
      System.exit(0);
    }
  }

  /* ------------------------------------------------------------- */

  TransformGroup createYRotation ()
  {
      TransformGroup objTrans = new TransformGroup();

      objTrans.setCapability (TransformGroup.ALLOW_TRANSFORM_WRITE);

      Transform3D yAxis = new Transform3D();
      Alpha rotationAlpha = new Alpha(-1, Alpha.INCREASING_ENABLE,
                                      0, 0,
                                      4000, 0, 0,
                                      0, 0, 0);

      RotationInterpolator rotator =
          new RotationInterpolator(rotationAlpha, objTrans, yAxis,
                                   0.0f, (float) Math.PI*2.0f);

      BoundingSphere bounds = new BoundingSphere(new Point3d(0.0,0.0,0.0), 100.0);

      rotator.setSchedulingBounds(bounds);

      objTrans.addChild (rotator);
      return objTrans;

  }

  TransformGroup createSlowYRotation ()
  {
      TransformGroup objTrans = new TransformGroup();
      objTrans.setCapability (TransformGroup.ALLOW_TRANSFORM_WRITE);

      Transform3D yAxis = new Transform3D();
      Alpha rotationAlpha = new Alpha(-1, Alpha.INCREASING_ENABLE,
                                      0, 0,
                                      16000, 0, 0,
                                      0, 0, 0);

      RotationInterpolator rotator =
          new RotationInterpolator(rotationAlpha, objTrans, yAxis,
                                   0.0f, (float) Math.PI*2.0f);

      BoundingSphere bounds = new BoundingSphere(new Point3d(0.0,0.0,0.0), 100.0);
      rotator.setSchedulingBounds(bounds);

      objTrans.addChild (rotator);
      return objTrans;
  }


  TransformGroup createLocRotation ()
  {
      TransformGroup objTrans = new TransformGroup();
      objTrans.setCapability (TransformGroup.ALLOW_TRANSFORM_WRITE);

      Transform3D yAxis = new Transform3D();
      Alpha rotationAlpha = new Alpha(-1, Alpha.INCREASING_ENABLE,
                                      0, 0,
                                      4000, 0, 0,
                                      0, 0, 0);

      RotationInterpolator rotator =
          new RotationInterpolator(rotationAlpha, objTrans, yAxis,
                                   0.0f, (float) Math.PI*2.0f);

      BoundingSphere bounds = new BoundingSphere(new Point3d(0.0,0.0,0.0), 100.0);
      rotator.setSchedulingBounds(bounds);

      objTrans.addChild (rotator);
      return objTrans;
  }


  TransformGroup createZRotation ()
  {
      TransformGroup objTrans = new TransformGroup();
      objTrans.setCapability (TransformGroup.ALLOW_TRANSFORM_WRITE);

      Transform3D zAxis = new Transform3D();
      zAxis.rotZ (Math.PI/2);

      Alpha rotationAlpha = new Alpha(-1, Alpha.INCREASING_ENABLE,
                                      0, 0,
                                      60000, 0, 0,
                                      0, 0, 0);

      RotationInterpolator rotator =
          new RotationInterpolator(rotationAlpha, objTrans, zAxis,
                                   0.0f, (float) Math.PI*2.0f);

      BoundingSphere bounds = new BoundingSphere(new Point3d(0.0,0.0,0.0), 100.0);
      rotator.setSchedulingBounds(bounds);

      objTrans.addChild (rotator);
      return objTrans;
  }


  TransformGroup createScale (double scale)
  {
      Transform3D t3d = new Transform3D ();
      t3d.setScale (scale);

      TransformGroup objTrans = new TransformGroup();
      objTrans.setTransform (t3d);
      return objTrans;
  }

  TransformGroup createTranslation (float x, float y, float z)
  {
      Transform3D t3d = new Transform3D ();
      t3d.setTranslation (new Vector3f (x, y, z));

      TransformGroup objTrans = new TransformGroup();
      objTrans.setTransform (t3d);
      return objTrans;
  }

  void link (TransformGroup target,
             float x, float y, float z,
             Shape3D node)
  {
     TransformGroup td = createTranslation (x, y, z);
     td.addChild (node);
     target.addChild (td);
  }

  void linkRot (TransformGroup target,
                float x, float y, float z,
                Shape3D node)
  {
     TransformGroup td = createTranslation (x, y, z);
     TransformGroup rot = createLocRotation ();

     rot.addChild (node);
     td.addChild (rot);
     target.addChild (td);
  }

  TransformGroup createManipulation ()
  {
      TransformGroup objTrans = new TransformGroup ();
      int a = 0;
      BoundingSphere bounds =
         new BoundingSphere(new Point3d(0.0,0.0,0.0), 100.0);

      // behavior
      objTrans.setCapability (TransformGroup.ALLOW_TRANSFORM_READ);
      objTrans.setCapability (TransformGroup.ALLOW_TRANSFORM_WRITE);

      // Create the rotate behavior node
      MouseRotate behavior = new MouseRotate (objTrans);
      objTrans.addChild (behavior);
      behavior.setSchedulingBounds (bounds);

      // Create the zoom behavior node
      MouseZoom behavior2 = new MouseZoom(objTrans);
      objTrans.addChild (behavior2);
      behavior2.setSchedulingBounds (bounds);

      // Create the translate behavior node
      MouseTranslate behavior3 = new MouseTranslate (objTrans);
      objTrans.addChild (behavior3);
      behavior3.setSchedulingBounds (bounds);

      return objTrans;
  }


  /* ------------------------------------------------------------- */

  BranchGroup createCube1 ()
  {
      BranchGroup objRoot = new BranchGroup();
      TransformGroup objTrans = createManipulation ();

      objTrans.addChild (new ColorCube(0.4));

      objRoot.addChild (objTrans);
      objRoot.compile();
      return objRoot;
  }

  BranchGroup createCube2 ()
  {
      BranchGroup objRoot = new BranchGroup ();
      TransformGroup objTrans = createYRotation ();

      objTrans.addChild (new ColorCube (0.4));

      objRoot.addChild (objTrans);
      objRoot.compile();
      return objRoot;
  }

  BranchGroup createDodecahedron ()
  {
      BranchGroup objRoot = new BranchGroup ();
      TransformGroup objScale = createScale (0.4);
      // TransformGroup objTrans = createYRotation ();
      TransformGroup objTrans = createManipulation ();

      objTrans.addChild (new Dodecahedron () );

      objScale.addChild (objTrans);
      objRoot.addChild (objScale);
      objRoot.compile();
      return objRoot;
  }

  BranchGroup createScene1 ()
  {
      BranchGroup objRoot = new BranchGroup ();
      TransformGroup objScale = createScale (0.2);
      TransformGroup objTrans = createYRotation ();

      float r = 2.4f;
      link (objTrans,  r, 0,  0, new Dodecahedron () );
      link (objTrans,  0, 0,  r, new Dodecahedron () );
      link (objTrans, -r, 0,  0, new Dodecahedron () );
      link (objTrans,  0, 0, -r, new Dodecahedron () );

      objScale.addChild (objTrans);
      objRoot.addChild (objScale);

      objRoot.compile();
      return objRoot;
  }

  BranchGroup createScene2 ()
  {
      BranchGroup objRoot = new BranchGroup ();
      TransformGroup objScale = createScale (0.2);
      TransformGroup objTemp = createZRotation ();
      TransformGroup objTrans = createSlowYRotation ();

      float r = 2.2f;
      linkRot (objTrans,  r, 0,  0, new Dodecahedron () );
      linkRot (objTrans,  0, 0,  r, new Dodecahedron () );
      linkRot (objTrans, -r, 0,  0, new Dodecahedron () );
      linkRot (objTrans,  0, 0, -r, new Dodecahedron () );

      objScale.addChild (objTrans);
      objTemp.addChild (objScale);
      objRoot.addChild (objTemp);

      objRoot.compile();
      return objRoot;
  }

  /* ------------------------------------------------------------- */

  BranchGroup createSphere ()
  {
      BranchGroup objRoot = new BranchGroup ();
      TransformGroup objScale = createScale (0.4);
      TransformGroup objTrans = createManipulation ();

      Sphere sphere = new Sphere (1.35f, Sphere.GENERATE_NORMALS, 100);

      TransparencyAttributes transp =
         new TransparencyAttributes (TransparencyAttributes.BLENDED, 0.1f);

      Material mat = new Material ();
      mat.setDiffuseColor (0.0f, 0.5f, 0.0f);
      mat.setEmissiveColor (0.0f, 0.5f, 0.0f);

      Appearance app = new Appearance ();
      app.setTransparencyAttributes (transp);
      app.setMaterial (mat);
      sphere.setAppearance (app);

      // cylinder

      Material mat2 = new Material ();
      mat2.setDiffuseColor (0.5f, 0.0f, 0.0f);
      mat2.setEmissiveColor (0.5f, 0.0f, 0.0f);
      // mat2.setSpecularColor (1.0f, 0.0f, 0.0f);
      // mat2.setAmbientColor (1.0f, 0.0f, 0.0f);

      Appearance app2 = new Appearance ();
      app2.setMaterial (mat2);

      float r = 0.4f;
      float h = 2.1f*1.11352f;
      Cylinder cylinder = new Cylinder (r, h, Cylinder.GENERATE_NORMALS, 50, 50, app2);

      BoundingSphere bounds =
         new BoundingSphere(new Point3d(0.0,0.0,0.0), 100.0);

      // light
      /*
      Color3f lColor1 = new Color3f (2.0f, 1.0f, 1.0f);
      Vector3f lDir1  = new Vector3f (-1.0f, -1.0f, -1.0f);
      DirectionalLight lgt1 = new DirectionalLight (lColor1, lDir1);
      lgt1.setInfluencingBounds (bounds);
      objScale.addChild (lgt1);

      Color3f lColor2 = new Color3f (1.0f, 1.0f, 2.0f);
      Vector3f lDir2  = new Vector3f (1.0f, 1.0f, -1.0f);
      DirectionalLight lgt2 = new DirectionalLight (lColor2, lDir2);
      lgt2.setInfluencingBounds (bounds);
      objScale.addChild (lgt2);

      Color3f lColor3 = new Color3f (1.0f, 1.0f, 1.0f);
      AmbientLight lgt3 = new AmbientLight (lColor3);
      lgt3.setInfluencingBounds (bounds);
      objScale.addChild (lgt3);

      Color3f lColor4 = new Color3f (1.0f, 1.0f, 2.0f);
      Point3f lPos4  = new Point3f (-10.0f, -10.0f, 10.0f);
      PointLight lgt4 = new PointLight ();
      lgt4.setColor (lColor4);
      lgt4.setPosition (lPos4);
      lgt4.setInfluencingBounds (bounds);
      objScale.addChild (lgt4);
      */

      Color3f lColor5 = new Color3f (1.0f, 1.0f, 1.0f);
      Point3f lPos5  = new Point3f (10.0f, 10.0f, 10.0f);
      PointLight lgt5 = new PointLight ();
      lgt5.setColor (lColor5);
      lgt5.setPosition (lPos5);
      lgt5.setInfluencingBounds (bounds);
      objScale.addChild (lgt5);

      TransformGroup objTrans2 = createManipulation ();
      // objTrans2.addChild (new Dodecahedron ());
      Transform3D trans = new Transform3D ();
      trans.setTranslation (new Vector3d (0, 0, 0));
      objTrans2.setTransform (trans);

      objTrans.addChild (cylinder);
      objScale.addChild (sphere);
      objScale.addChild (objTrans);
      objScale.addChild (objTrans2);
      objRoot.addChild (objScale);
      objRoot.compile();
      return objRoot;
  }

}



