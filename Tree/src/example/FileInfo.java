package example;

import java.io.*;

public class FileInfo {
    
    private File f;
    
    public FileInfo (File f0)
    {
        f = f0;
    }
    
    public String toString ()
    {
        return f.getName () + " " + f.length();
    }
    
}
