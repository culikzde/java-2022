package example;

import javax.swing.*;
import javax.swing.table.*;
import javax.swing.tree.*;
import java.awt.*;
import java.io.*;
import java.util.*;
import java.lang.reflect.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Window extends javax.swing.JFrame {

    public Window() {
        initComponents();
        // showTree ();
        // showDirectory ();
        showNumbers ();
        
        showWidgets (jButton1);
    }
    
    private void showWidgets (Component obj)
    {
        while (obj.getParent() != null)
            obj = obj.getParent();

        DefaultMutableTreeNode root = new DefaultMutableTreeNode (obj);
        showObjects (root, obj);
        
        DefaultTreeModel model = new DefaultTreeModel (root);
        tree.setModel (model);
    }
    
    private void showObjects (DefaultMutableTreeNode target, Component obj)
    {
        if (obj instanceof Container)   
        {
            Container c = (Container) obj;
            int cnt = c.getComponentCount();
            for (int i = 0; i < cnt; i++)
            {
                Component item = c.getComponent(i);
                // System.out.println ("ITEM " + item);
                DefaultMutableTreeNode node = new DefaultMutableTreeNode (item);
                target.add (node);
                showObjects (node, item);
            }
        }
    }
       
    private void showTree ()
    {
        DefaultMutableTreeNode root = new DefaultMutableTreeNode ("root");
        DefaultMutableTreeNode branch = new DefaultMutableTreeNode ("branch");
        DefaultMutableTreeNode list = new DefaultMutableTreeNode ("list");
        
        root.add (branch);
        branch.add (list);
        
        DefaultTreeModel model = new DefaultTreeModel (root);
        tree.setModel (model);
    }
    
    private void showSubDir (DefaultMutableTreeNode target, File dir)
    {
        File [] files = dir.listFiles();
        
        for (File file : files)
        {
           String name = file.getName();
           FileInfo info = new FileInfo (file);
           DefaultMutableTreeNode node = new DefaultMutableTreeNode (info);
           target.add (node);
           if (file.isDirectory())
               showSubDir (node, file);
        }
    }
    private void showDirectory ()
    {
        File dir = new File (".");
        DefaultMutableTreeNode root = new DefaultMutableTreeNode (dir.getAbsolutePath());
        showSubDir (root, dir);
        
        DefaultTreeModel model = new DefaultTreeModel (root);
        tree.setModel (model);
    }
    
    private void showNumbers ()
    {
        double [] [] a = { {10, 20, 30}, { 200, 314.159} };
               
        int lines = a.length;
        
        int columns = 0;
        for (int i =0 ; i < a.length; i++)
            if (a[i].length > columns)
                 columns = a[i].length;
        
        Object [] [] p = new Object [lines][columns];
        
        for (int i = 0 ; i < lines; i++)
            for (int k = 0 ; k < columns; k++)
                if (k < a[i].length)
                    p[i][k] = a[i][k];
                else
                    p[i][k] = "";
        
        String [] title = new String [columns];
        for (int k = 0 ; k < columns; k++)
            title [k] = "" + (char) ('A' + k);
        
        TableModel m = new DefaultTableModel (p, title);
        
        jTable1.setModel (m);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        tree = new javax.swing.JTree();
        jButton1 = new javax.swing.JButton();
        methodField = new javax.swing.JTextField();
        paramField = new javax.swing.JTextField();
        resultField = new javax.swing.JTextField();
        jButton2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        tree.addTreeSelectionListener(new javax.swing.event.TreeSelectionListener() {
            public void valueChanged(javax.swing.event.TreeSelectionEvent evt) {
                treeValueChanged(evt);
            }
        });
        jScrollPane2.setViewportView(tree);

        jButton1.setText("jButton1");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        methodField.setText("setText");

        paramField.setText("ABC");
        paramField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                paramFieldActionPerformed(evt);
            }
        });

        resultField.setText("jTextField1");

        jButton2.setText("jButton2");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 327, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, 325, Short.MAX_VALUE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(paramField)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 325, Short.MAX_VALUE)
                        .addComponent(methodField)
                        .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, 325, Short.MAX_VALUE)
                        .addComponent(resultField)))
                .addContainerGap(276, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 360, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(methodField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(paramField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(16, 16, 16)
                        .addComponent(resultField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton1))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 529, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton2)
                .addGap(60, 60, 60))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void treeValueChanged(javax.swing.event.TreeSelectionEvent evt) {//GEN-FIRST:event_treeValueChanged
       DefaultMutableTreeNode node = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
       if (node != null)
       {
           // System.out.println ("CLICK " + node);
           displayProperties (node.getUserObject());
       }
    }//GEN-LAST:event_treeValueChanged

    private void paramFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_paramFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_paramFieldActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        Object obj = display_object; 
        String name = methodField.getText();
        String value = paramField.getText();
        
        Class cls = obj.getClass();
        Class [] param_types = { String.class };
        Object result = "";
        try
        {
           Method m = cls.getMethod (name, param_types);
           Object [] param_values = { value };
           result = m.invoke (obj, param_values);
        }
        catch (Exception ex)
        {
            result = "Exception " + ex.toString();
        }
        resultField.setText ("" + result);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        Object obj = null;
        try
        {
           Class cls = Class.forName ("javax.swing.JTable");
           obj = cls.newInstance();
        }
        catch (Exception ex)
        {
            resultField.setText ("Exception " + ex.toString());
        }
        displayProperties(obj);
        if (obj instanceof JTable)
        {
            JTable w = (JTable) obj;
            w.show ();
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    DefaultTableModel model;
    
    private void addLine (String name, Object value)
    {
        Object [] line = { name, value };
        model.addRow (line);
    }

    Object display_object = null;
    
    private void displayProperties (Object obj)
    {
        display_object = obj;
        
        model = new DefaultTableModel ();
        model.addColumn ("name");
        model.addColumn ("value");
        
        String s = "";
        s = this.toString();
        
        addLine ("toString", obj.toString());
        addLine ("getClass", obj.getClass ().getName());
        // addLine ("getClass", obj.getClass ().getClass().getName());
        
        Class cls = obj.getClass();
        Method [] methods = cls.getMethods();
        for (Method m : methods)
        {
            Object value = "";
            if (m.getParameterCount() == 0 && m.getName().startsWith("get"))
            {
                try 
                {
                    value = m.invoke (obj, new Object [] {});
                } 
                catch (Exception ex) 
                {
                    value = "Exception " + ex.toString();
                }
            }
            addLine (m.getName(), value);
        }
        
        jTable1.setModel (model);
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Window.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Window.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Window.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Window.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Window().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField methodField;
    private javax.swing.JTextField paramField;
    private javax.swing.JTextField resultField;
    private javax.swing.JTree tree;
    // End of variables declaration//GEN-END:variables
}
