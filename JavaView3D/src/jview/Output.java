package jview;

public class Output {
    
    public void send (String s) { }

    public void putChar (String s) { }
    public void putString (String s) { }

    public void style_no_space () { }
    public void style_new_line () { }
    public void style_empty_line () { }
    public void style_indent () { }
    public void style_unindent () { }
    
}
