
import org.alice.apis.moveandturn.event.MouseButtonEvent;
import org.alice.apis.moveandturn.event.MouseButtonListener;
import org.alice.apis.moveandturn.*;
import org.alice.virtualmachine.ForEachRunnable;
import org.alice.virtualmachine.DoTogether;
import org.alice.virtualmachine.ForEachTogether;
import org.alice.apis.moveandturn.gallery.shapes.*;
import org.alice.apis.moveandturn.gallery.shapes.Sphere;

public class MyScene extends Scene {

    private MySunLight sunLight = new MySunLight();
    private MyGrassyGround grassyGround = new MyGrassyGround();
    private MyCamera camera = new MyCamera();
    private MyBunny2 bunny2 = new MyBunny2();
    private MyCheshireCat cheshireCat = new MyCheshireCat();

    public MyScene() {
        this.performGeneratedSetUp();
        this.performMySetUp();
    }

    protected void performMySetUp() {
    }

    protected void performGeneratedSetUp() {
        this.setName("scene");
        this.setAtmosphereColor(new Color(0.5, 0.5, 1.0));
        this.sunLight.setName("sunLight");
        this.sunLight.setVehicle(this);
        this.sunLight.setLocalPointOfView(new PointOfView(new Quaternion(-0.7071067811865475, 0.0, 0.0, 0.7071067811865476), new Position(0.0, 0.0, 0.0)));
        this.grassyGround.setName("grassyGround");
        this.grassyGround.setVehicle(this);
        this.grassyGround.setLocalPointOfView(new PointOfView(new Quaternion(0.0, 0.0, 0.0, 1.0), new Position(0.0, 0.0, -0.0)));
        this.grassyGround.setColor(new Color(1.0, 1.0, 1.0));
        this.grassyGround.setOpacity(1.0);
        this.camera.setName("camera");
        this.camera.setVehicle(this);
        this.camera.setLocalPointOfView(new PointOfView(new Quaternion(-0.06459732570111666, 0.9577147468110553, 0.19313996024056168, 0.20324026424696034), new Position(2.7844840398330084, 2.0022116377164663, -5.241320824605207)));
        this.bunny2.setName("bunny2");
        this.bunny2.setVehicle(this);
        this.bunny2.setLocalPointOfView(new PointOfView(new Quaternion(0.0, 0.0, 0.0, 1.0), new Position(1.6253201146605338, -1.3877787807814457E-17, -0.38048390392057563)));
        this.bunny2.setColor(new Color(1.0, 1.0, 1.0));
        this.bunny2.setOpacity(1.0);
        this.cheshireCat.setName("cheshireCat");
        this.cheshireCat.setVehicle(this);
        this.cheshireCat.setLocalPointOfView(new PointOfView(new Quaternion(0.0, -0.20260913803244265, 0.0, 0.9792596883287653), new Position(-2.585723146017395, 0.403779878664944, -0.17911280286699682)));
        this.cheshireCat.setColor(new Color(1.0, 1.0, 1.0));
        this.cheshireCat.setOpacity(1.0);
    }

    public void run() {
        
        new Thread() {
            public void run() {
                while (true) {
                    MyScene.this.cheshireCat.turn(TurnDirection.LEFT, 0.25);
                    MyScene.this.cheshireCat.moveToward(0.25, MyScene.this.bunny2);
                }
            }
        }.start();
        
        
        this.bunny2.addMouseButtonListener(new MouseButtonListener() {
            public void mouseButtonClicked(final MouseButtonEvent e) {
                // MyScene.this.bunny2.move(MoveDirection.UP, 1.0);
                
                Sphere sphere = new Sphere ();
                sphere.setName("sphere");
                sphere.setVehicle(bunny2);
                sphere.setColor(new Color(0.0, 0.0, 1.0));
                sphere.setOpacity(0.5);
                
                // MyScene.this.bunny2.say("Click");
                // MyScene.this.bunny2.move(MoveDirection.DOWN, 1.0);

            }
        });
        while (true) {
            {
                this.bunny2.move(MoveDirection.FORWARD, 1.0);
                this.bunny2.turn(TurnDirection.RIGHT, 0.25);
            }
        }
        // this.bunny2.say("hello");
    }
}
